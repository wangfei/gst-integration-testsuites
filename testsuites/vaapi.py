# -*- Mode: Python -*- vi:si:et:sw=4:sts=4:ts=4:syntax=python
#
# Copyright (C) 2017 Intel Corporation
# Copyright (c) 2017 Hyunjun Ko <zzoon@igalia.com>
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation; either
# version 2.1 of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with this program; if not, write to the
# Free Software Foundation, Inc., 51 Franklin St, Fifth Floor,
# Boston, MA 02110-1301, USA.

"""
The VA-API GstValidate default testsuite
"""

TEST_MANAGER = "vaapi"

import os
import sys
import urllib.parse
from launcher.apps.gstvalidate import GstValidateTestManager, GstValidatePipelineTestsGenerator, GstValidatePlaybinTestsGenerator, GstValidateTranscodingTestsGenerator, GstValidateTranscodingTest

BLACKLIST_VAAPI = [('svg',
              'This is an image'
             ' (Deactivating as it is not supported for VA-API.)'),
             ('png',
              'This is an image'
             ' (Deactivating as it is not supported to VA-API.)'),
             ('ogg',
              'This is an audio'
             ' (Deactivating as it is not related to VA-API.)'),
             ('http',
              'Deactivating all http testcases'
             ' (Deactivating as it is not related to VA-API.)')
            ]

class VAAPITestsManager(GstValidateTestManager):
    name = "vaapi"
    GstValidatePlaybinTestsGenerator = GstValidatePlaybinTestsGenerator
    GstValidateTranscodingTestsGenerator = GstValidateTranscodingTestsGenerator
    GstValidateTranscodingTest = GstValidateTranscodingTest

    def __init__(self):
        super(VAAPITestsManager, self).__init__()

    def init(self):
        return True

    def add_options(self, parser):
        group = parser.add_argument_group("GStreamer VA-API specific option"
                            " and behaviours",
                            description="""
The GStreamer VA-API launcher will be usable only if GStreamer VA-API has been compiled against GstValidate
You can simply run as the following:

    $gst-validate-launcher vaapi
""")

    def register_encoding_formats(self):
        """
        Registers encoding formats
        """
        self.add_encoding_formats([
            MediaFormatCombination("webm", "vorbis", "vp8"),
            MediaFormatCombination("mp4", "mp3", "h264"),
            MediaFormatCombination("mkv", "vorbis", "h264"),
        ])

    def register_defaults(self, project_paths=None):
        if self._default_generators_registered:
            return

        self.add_scenarios([
            "play_15s",
            "fast_backward",
            "fast_forward",
            "reverse_playback",
            "seek_backward",
            "seek_forward",
            "seek_with_stop",
            ])

        self.add_generators([GstValidatePlaybinTestsGenerator(self),
                             GstValidateTranscodingTestsGenerator(self)])
        self.register_default_blacklist()
        self.set_default_blacklist(BLACKLIST_VAAPI)
        self.register_encoding_formats()
        self._default_generators_registered = True

    def set_settings(self, options, args, reporter):
        options.disable_rtsp = True
#        options.add_paths(os.path.abspath("/root/gst-validate/gst-integration-testsuites/medias/defaults"))
        assets = os.environ.get("ROOT_PATH_MEDIA_GST_VALIDATE_ASSETS", "/root/gst-validate")
        assets = os.path.join(assets, "gst-integration-testsuites", "medias", "defaults")
        options.add_paths(os.path.abspath(assets))

        if not options.wanted_tests:
            options.wanted_tests = ["file"]

        super(GstValidateTestManager, self).set_settings(options, args, reporter)


def setup_tests(test_manager, options):
    if not options.wanted_tests:
        options.wanted_tests = ["file"]
    test_manager.register_defaults()
    return True
